import Vue from "vue";
import App from "./App.vue";
import _CClima from "./clases/CClima";

declare global {
  interface Window {
    CClima: _CClima;
  }
}

var items = new _CClima();
console.log(items);
new Vue({
  render: (h) => h(App),
}).$mount("#app");
