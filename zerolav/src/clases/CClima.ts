export default class CClima {
  id: number;
  title: string;
  estoesunaclase: string;

  constructor() {
    this.id = 0;
    this.title = "vacio";
    this.estoesunaclase = "";
  }

  // Comportamiento
  toString() {
    return `[id: ${this.id}, title: ${this.title}]`;
  }
}
